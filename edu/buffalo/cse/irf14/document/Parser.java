/**
 * 
 */
package edu.buffalo.cse.irf14.document;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * @author nikhillo
 * Class that parses a given file into a Document
 */
public class Parser {
	/**
	 * Static method to parse the given file into the Document object
	 * @param filename : The fully qualified filename to be parsed
	 * @return The parsed and fully loaded Document object
	 * @throws ParserException In case any error occurs during parsing
	 */
	public static Document parse(String filename) throws ParserException {
		// TODO YOU MUST IMPLEMENT THIS

		Document d = new Document();
		if(filename==null){
			throw new ParserException();
		}

		try{
			File file = new File(filename);

			if(!file.exists()){
				throw new ParserException();
			}

			FileReader freader = new FileReader(file);
			BufferedReader breader = new BufferedReader(freader);
			String line = null;
			StringBuilder content_buffer = new StringBuilder();
			String t = new String();

			Boolean parsed_title = true;
			Boolean parsed_date = true;
			Boolean parsed_author = true;

			//apparently, unique file names have already been given to us, so no need for this :)
			//increment the atomicinteger id to get a unique doument id as string
			// d.doc_id.incrementAndGet();
			// d.setField(FieldNames.FILEID, d.doc_id.toString());

			d.setField(FieldNames.FILEID, file.getName());

			//set name of parent folder as the category
			d.setField(FieldNames.CATEGORY, file.getParentFile().getName());

			while((line = breader.readLine()) != null){
			if(!line.isEmpty()){

					//title parsing
					if(parsed_title){
						d.setField(FieldNames.TITLE,line);
						parsed_title = false;
					}

					// author and authorg parsing
					else if(line.contains("<AUTHOR>")){

						String[] author_meta = line.replace("<AUTHOR>","").replace("</AUTHOR>", "").trim().substring(2).split(",");
						d.setField(FieldNames.AUTHOR, author_meta[0].trim());
						if(author_meta.length>1){
							d.setField(FieldNames.AUTHORORG,author_meta[author_meta.length-1].trim());
						}
						
						parsed_author = false;
					}

					// date and place parsing
					else if(parsed_date){
						if(line.contains(" - ")){

						String[] sects = line.split(" - ");

						String[] place_and_date = sects[0].split(",");						
						String[] place = Arrays.copyOfRange(place_and_date,0,(place_and_date.length-1));


						//very crude solution to a very crude issue
						for(int index = 0; index<place.length;index++)
						{
							place[index] = place[index].trim();
						}

						StringBuilder temp = new StringBuilder();


						//extremely risky, but works for now
						if(place.length>0){
							temp.append(place[0].trim());

						if(place.length>1){

							for(int i = 1; i<place.length; i++){
								temp.append(", " + place[i].trim());
							}
						}
						}

						d.setField(FieldNames.PLACE, temp.toString());
						d.setField(FieldNames.NEWSDATE,place_and_date[place_and_date.length-1].trim());

						// if place_date metadata doesn't occupy the whole line, push the rest of the text to content
						if(sects.length>1){
							content_buffer.append(sects[1]);
						}
						}
						parsed_date = false;

					}
					// finally, content picks up the crumbs....
					else{
							content_buffer.append(line.trim()+" ");
						}
					}
				}
		
			breader.close();
			freader.close();

			// ... and eats them up at this point
			d.setField(FieldNames.CONTENT, content_buffer.toString());
			}
		catch(IOException ex){
			ex.printStackTrace();
		}

		return d;
}
}