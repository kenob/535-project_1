/**
 * 
 */
package edu.buffalo.cse.irf14.index;

import edu.buffalo.cse.irf14.document.Document;
import edu.buffalo.cse.irf14.document.FieldNames;
import edu.buffalo.cse.irf14.analysis.*;

import java.util.*;
import java.io.*;


/**
 * @author nikhillo
 * Class responsible for writing indexes to disk
 */
public class IndexWriter {
	private class MasterIndex{
		public TreeMap<String, TreeSet<String>> midlevel_index = new TreeMap<String, TreeSet<String>>();
		public TreeMap<Integer, TreeSet<String>> final_index = new TreeMap<Integer, TreeSet<String>>();

	}
	String out_dir;

	
	//index type dictionaries
	//might need to implement better data structures for these
	static TreeMap<Integer, String> document_dictionary;

	static TreeMap<String, Integer> term_dictionary = new TreeMap<String, Integer>();
	static HashMap<Integer, String> author_dictionary;
	static HashMap<Integer, String> category_dictionary;
	static HashMap<Integer, String> place_dictionary;
	//static TreeMap<String, TreeSet<String>> master_index = new TreeMap<String, TreeSet<String>>();
	static int document_id;
	static Map<FieldNames, IndexType> fielld_index_map = new HashMap<FieldNames, IndexType>();
	FieldNames[] field_type = {FieldNames.CATEGORY, FieldNames.TITLE, FieldNames.AUTHOR, FieldNames.AUTHORORG, FieldNames.PLACE, FieldNames.CONTENT};
	HashMap<IndexType, MasterIndex> type_index_map = new HashMap<IndexType, IndexWriter.MasterIndex>();

	/**
	 * Default constructor
	 * @param indexDir : The root directory to be sued for indexing
	 */
	public IndexWriter(String indexDir) {
		//TODO : YOU MUST IMPLEMENT THIS
		out_dir = indexDir;
		fielld_index_map.put(FieldNames.AUTHOR, IndexType.AUTHOR);
		fielld_index_map.put(FieldNames.AUTHORORG, IndexType.TERM);
		fielld_index_map.put(FieldNames.CATEGORY, IndexType.CATEGORY);
		fielld_index_map.put(FieldNames.CONTENT, IndexType.TERM);
		fielld_index_map.put(FieldNames.FILEID, IndexType.TERM);
		fielld_index_map.put(FieldNames.NEWSDATE, IndexType.TERM);
		fielld_index_map.put(FieldNames.PLACE, IndexType.PLACE);
		fielld_index_map.put(FieldNames.AUTHORORG, IndexType.TERM);
		if(document_dictionary==null){
			document_dictionary = new TreeMap<Integer,String>();
			document_id = 0;
		}
		type_index_map.put(IndexType.TERM, new MasterIndex()); 
		type_index_map.put(IndexType.CATEGORY, new MasterIndex()); 
		type_index_map.put(IndexType.PLACE, new MasterIndex()); 
		type_index_map.put(IndexType.AUTHOR, new MasterIndex()); 
		

		
	}
	/**
	 * Method to add the given Document to the index
	 * This method should take care of reading the filed values, passing
	 * them through corresponding analyzers and then indexing the results
	 * for each indexable field within the document. 
	 * @param d : The Document to be added
	 * @throws IndexerException : In case any error occurs
	 */

	public void addDocument(Document d) throws IndexerException {
		//TODO : YOU MUST IMPLEMENT THIS
		// document_dictionary.put(document_id, d.getField(FieldNames.FILEID));



		//for each type to be indexed
		for(FieldNames field_name : field_type){
				IndexType index_type = fielld_index_map.get(field_name);
				//for some reason, getfield returns a list, so join the elements instead of just getting the first element, for the benefit of doubt
				StringBuilder joined_field = new StringBuilder();
				String[] res = {};
				String[] pre = d.getField(field_name);

				if(!(pre==null)){

					for(String j : pre){
						joined_field.append(j + " ");
					}

					try{
						 res = tokenize_and_analyze(field_name, joined_field.toString());
					}

					catch(TokenizerException t){
						t.printStackTrace();
					}

					//***dictionary to store the terms and counts for each term in a single document. Probably not useful for now
					TreeMap<String, Integer> local_index_type_dictionary = new TreeMap<String, Integer>();
					for(String term: res){

						//see *** above
						if(local_index_type_dictionary.keySet().contains(term)){
							local_index_type_dictionary.put(term, local_index_type_dictionary.get(term)+1);
						}
						else{
							local_index_type_dictionary.put(term, 1);
						}

					}
					/*finally, update the "global" postings list for this index type with information from the local_list,
					 update the global look-up table for this type,
					 and write the postings list to disk
					*/
					for(String key : local_index_type_dictionary.keySet()){
						 //proposed term with meta to store term and number of times it appeared in this document. delimiter would be '/' in this case
						String term_with_meta = local_index_type_dictionary.get(key) + "/" + key;

						 //TODO: if fieldname exists in index types, create the lookup dictionary and a special index




						 /*this method should probably read the index file for this type from disk and update it
						   not entirely convinced about the efficiency. It ensures that we never run out of memory, though. Overkill??
						   SUGGESTION: Keep a static counter, and only write files to disk in batches of say, 10, as long as close() has not been called*/
						
						updatePostingsList(index_type, term_with_meta, document_id);

					}
				}
		}
		document_dictionary.put(document_id, d.getField(FieldNames.FILEID)[0]);
		document_id += 1;

	}

	
	/**
	 * Method that indicates that all open resources must be closed
	 * and cleaned and that the entire indexing operation has been completed.
	 * @throws IndexerException : In case any error occurs
	 */
	public void close() throws IndexerException {
		//TODO

		//VERY IMPORTANT: Close index file(s)
		//write index to disk and dereference


		//create final dictionaries and indexes here, and write them to disk


		ArrayList<String> t = new ArrayList<String>();
		for (IndexType type :IndexType.values())
		{
			t.addAll(type_index_map.get(type).midlevel_index.keySet());
		}
		
		MasterIndex mi = null;
		//creating the term dictionary
		for (IndexType type :IndexType.values())
		{
			mi = type_index_map.get(type);
			for(int i = 0; i < t.size(); i++){
				String g = t.get(i);
				mi.final_index.put(i, mi.midlevel_index.get(g));
				term_dictionary.put(t.get(i), i);
			}
		}
		
		TreeMap<Integer, TreeSet<String>> fi_term = type_index_map.get(IndexType.TERM).final_index;
		TreeMap<Integer, TreeSet<String>> fi_author = type_index_map.get(IndexType.AUTHOR).final_index;
		TreeMap<Integer, TreeSet<String>> fi_category = type_index_map.get(IndexType.CATEGORY).final_index;
		TreeMap<Integer, TreeSet<String>> fi_place = type_index_map.get(IndexType.PLACE).final_index;
		
		writeToDisk(fi_term, "term", "ind");
		writeToDisk(fi_author, "author", "ind");
		writeToDisk(fi_category, "category", "ind");
		writeToDisk(fi_place, "place", "ind");
		writeToDisk(document_dictionary, "document", "dict");
		writeToDisk(term_dictionary, "term", "dict");

		mi = null;
		term_dictionary = null;
		document_dictionary = null;
		document_id = 0;

	}
	protected String[] tokenize_and_analyze(FieldNames f, String str) throws TokenizerException{
			Tokenizer tkizer = new Tokenizer();
			TokenStream tstream = tkizer.consume(str);
			AnalyzerFactory factory = AnalyzerFactory.getInstance();
			Analyzer filter = factory.getAnalyzerForField(f, tstream);

			while (filter.increment()) {
					//Do nothing :/
				}
				tstream = filter.getStream();
				tstream.reset();
				ArrayList<String> list = new ArrayList<String>();
				String s;
				Token t;

				while (tstream.hasNext()) {
					t = tstream.next();

					if (t != null) {
						s = t.toString();
						if (s!= null && !s.isEmpty())
							list.add(s);	
					}
				}				
				String[] rv = new String[list.size()];
				rv = list.toArray(rv);
				tkizer = null;
				tstream = null;
				filter = null;
				list = null;
				return rv;
		}

	protected boolean updatePostingsList(IndexType index_type, String term_, int document_id){
		String[] term = term_.split("/");
		MasterIndex tmp = type_index_map.get(index_type);
		if(tmp.midlevel_index.isEmpty() || !tmp.midlevel_index.containsKey(term[1])){
			TreeSet<String> temp = new TreeSet<String>();
			temp.add(document_id + "/" + term[0]);
			tmp.midlevel_index.put(term[1], temp);
			return true;
		}
		else {
			TreeSet<String> temp = tmp.midlevel_index.get(term[1]); 
			temp.add(document_id + "/" + term[0]);
			tmp.midlevel_index.put(term[1], temp);
			return true;
		}
	}

	protected boolean writeToDisk(Object o, String object, String type){
		try{
		File out_file = new File(out_dir, object+"."+type);
		FileOutputStream out_stream = new FileOutputStream(out_file);
		ObjectOutputStream ob = new ObjectOutputStream(out_stream);
		ob.writeObject(o);
		ob.close();
		out_stream.close();
		return true;
		}
		catch(IOException io){
			io.printStackTrace();
			return false;
		}

	}

	// protected HashMap<Integer,String> updateDictionary(TreeMap terms){
	// 	// HashMap<Integer, String> dict = new HashMap<Integer, String>();
	// 	ArrayList<String> t = new ArrayList(terms.keySet());
	// 	for(int i = 0; i < t.size()){
	// 		dict.put(i, t.get(i));
	// 	}
	// 	return dict;
	// }

}
