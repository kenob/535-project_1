/**
 * 
 */
package edu.buffalo.cse.irf14.index;

import java.util.List;
import java.util.Map;
import java.io.*;
import java.util.*;

/**
 * @author nikhillo
 * Class that emulates reading data back from a written index
 */
public class IndexReader {
	/**
	 * Default constructor
	 * @param indexDir : The root directory from which the index is to be read.
	 * This will be exactly the same directory as passed on IndexWriter. In case 
	 * you make subdirectories etc., you will have to handle it accordingly.
	 * @param type The {@link IndexType} to read from
	 */

	private TreeMap<Integer, TreeSet<String>> term_index;
	private TreeMap<String, Integer> term_dictionary;
	private TreeMap<Integer, String> document_dictionary;


	public IndexReader(String indexDir, IndexType type) {
		//TODO
		File in_dir = null;

		//Just opening the index and dictionary files. some (re)factoring required.
		try{
			in_dir = new File(indexDir, type.toString().toLowerCase()+".ind");
			FileInputStream fin = new FileInputStream(in_dir);
			ObjectInputStream obj = new ObjectInputStream(fin);

			try{
				term_index = (TreeMap<Integer, TreeSet<String>>) obj.readObject();
			}
			catch(ClassNotFoundException cl){
				cl.printStackTrace();
			}
			obj.close();
			fin.close();

			in_dir = new File(indexDir, type.toString().toLowerCase()+".dict");
			fin = new FileInputStream(in_dir);
			obj = new ObjectInputStream(fin);

			try{
				term_dictionary = (TreeMap<String, Integer>) obj.readObject();
			}
			catch(ClassNotFoundException cl){
				cl.printStackTrace();
			}

			obj.close();
			fin.close();

			in_dir = new File(indexDir, "document"+".dict");
			fin = new FileInputStream(in_dir);
			obj = new ObjectInputStream(fin);

			try{
				document_dictionary = (TreeMap<Integer, String>) obj.readObject();
			}
			catch(ClassNotFoundException cl){
				cl.printStackTrace();
			}

			obj.close();
			fin.close();
		}

		catch(IOException io){
			io.printStackTrace();
		}



	}
	
	/**
	 * Get total number of terms from the "key" dictionary associated with this 
	 * index. A postings list is always created against the "key" dictionary
	 * @return The total number of terms
	 */
	public int getTotalKeyTerms() {
		//TODO : YOU MUST IMPLEMENT THIS
		return term_dictionary.size();
	}
	
	/**
	 * Get total number of terms from the "value" dictionary associated with this 
	 * index. A postings list is always created with the "value" dictionary
	 * @return The total number of terms
	 */
	public int getTotalValueTerms() {
		//TODO: YOU MUST IMPLEMENT THIS
		return document_dictionary.size();
	}
	
	/**
	 * Method to get the postings for a given term. You can assume that
	 * the raw string that is used to query would be passed through the same
	 * Analyzer as the original field would have been.
	 * @param term : The "analyzed" term to get postings for
	 * @return A Map containing the corresponding fileid as the key and the 
	 * number of occurrences as values if the given term was found, null otherwise.
	 */
	public Map<String, Integer> getPostings(String term) {
		//TODO:YOU MUST IMPLEMENT THIS

		//find term in term dictionary, get its postings, lookup fileids. This method can't help its simplicity
		if(term_dictionary.containsKey(term)){
			HashMap<String, Integer> res = new HashMap<String, Integer>();
			int term_id = term_dictionary.get(term);
			for(String i : term_index.get(term_id)){
				int doc_id = Integer.parseInt(i.split("/")[0]);
				int freq = Integer.parseInt(i.split("/")[1]);

				res.put(document_dictionary.get(doc_id), freq);
			}
			return res;
		}
		else{
			return null;
		}
		
	}
	
	/**
	 * Method to get the top k terms from the index in terms of the total number
	 * of occurrences.
	 * @param k : The number of terms to fetch
	 * @return : An ordered list of results. Must be <=k fr valid k values
	 * null for invalid k values
	 */
	public List<String> getTopK(int k) {
		//TODO YOU MUST IMPLEMENT THIS

		//sort term index by number of terms and return first k
		if(k>0){
			ArrayList<String> str_res = new ArrayList<String>();
			ArrayList<Integer> int_result = sort_by_size(term_index, true);
			ArrayList<String> keys = new ArrayList<String>();

			for(String key : term_dictionary.keySet()){
				keys.add(key);
			}

			for(int i = 0; i<k; i++){
				if(i<int_result.size()){
					String n = keys.get(int_result.get(i));
					str_res.add(n);
				}
				else{
					break;
				}
			}
			return str_res;
		}

		else{
		return null;
		}
	}
	
	/**
	 * Method to implement a simple boolean AND query on the given index
	 * @param terms The ordered set of terms to AND, similar to getPostings()
	 * the terms would be passed through the necessary Analyzer.
	 * @return A Map (if all terms are found) containing FileId as the key 
	 * and number of occurrences as the value, the number of occurrences 
	 * would be the sum of occurrences for each participating term. return null
	 * if the given term list returns no results
	 * BONUS ONLY
	 */
	public Map<String, Integer> query(String...terms) {
		//TODO : BONUS ONLY

		//result postings

		HashMap<String, Integer> final_res = new HashMap<String, Integer>();
		ArrayList<Integer> term_ids = new ArrayList<Integer>();
		ArrayList<Integer> doc_ids = new ArrayList<Integer>();
		LengthComparator l = new LengthComparator(term_index, false);

		//treemap sorted by increasing order of document frequency
		TreeMap<Integer, TreeSet<String>> tee = new TreeMap<Integer, TreeSet<String>>(l);

		//make sure all terms exist in the dictionary, to start with
		for(String term : terms){

			if(!term_dictionary.containsKey(term)){
				return null;
			}
			int term_id = term_dictionary.get(term);
			term_ids.add(term_id);

		}

		//make a new postings list consisting of the search terms only and sorted in ascending order of appearances
		for(Integer i : term_ids){
				tee.put(i, term_index.get(i));
		}


		//interim doc-frequency map holds interim values for doc ids and frequencies
		HashMap<Integer, Integer> res = new HashMap<Integer, Integer>();


		//populate doc-frequency map from the first element(the element with the smallest postings list)
		Iterator i = tee.keySet().iterator();
		for(String j : tee.get(i.next())){
			String[] j_split = j.split("/");

			//add each document in which this term occurs to the interim results list, along with frequency
			res.put(Integer.parseInt(j_split[0]), Integer.parseInt(j_split[1]));
		}


		while(i.hasNext()){
			//filter and update the doc-frequency map recursively while there are still terms in the postings list

			int currKey = (Integer) i.next();

			//map to store the temporary doc-frequency map for this term
			HashMap<Integer, Integer> temp_res = new HashMap<Integer, Integer>();

			ArrayList<String> keys =  new ArrayList<String>();

			keys.addAll(tee.get(currKey));

			/*iterate over the set of postings for this and create a filtered version of document_id list 
			consisting of doc_ids that appear in this document*/

			for(String d : keys){
				int doc_id = Integer.parseInt(d.split("/")[0]);
				int freq = Integer.parseInt(d.split("/")[1]);

				//if this document exists in the result postings list, add it to the temporary posting list
				if(res.containsKey(doc_id)){
					temp_res.put(doc_id, res.get(doc_id) + freq);
				}

			}

			//overwrite the current document_id list with filtered version
			res = temp_res;		
			}


			//convert the doc-frequency map's keys to actual file names if any doc_ids survived
			if(res.size() > 0){
				for(int k : res.keySet()){
					String fileid = document_dictionary.get(k);
					final_res.put(fileid, res.get(k));
				}
				return final_res;
			}
			else{
				return null;
			}
	}

	protected ArrayList<Integer> sort_by_size(TreeMap<Integer, TreeSet<String>> t, boolean descending){
		LengthComparator l = new LengthComparator(t, descending);
		TreeMap<Integer, TreeSet> tee = new TreeMap<Integer, TreeSet>(l);
		tee.putAll(t);
		ArrayList<Integer> res = new ArrayList<Integer>();
		for(int i : tee.keySet()){
			res.add(i);
		}
		return res;
	}

	//utility for debugging
	protected void printmap(TreeMap k){
		for(Object i:k.keySet()){
			System.out.println(i.toString() + "---" + k.get(i));
		}
	}
}

/* To rank terms by frequency*/
class LengthComparator implements Comparator<Object>{
	TreeMap<Integer, TreeSet<String>> this_map;
	boolean descending;
	protected LengthComparator(TreeMap<Integer, TreeSet<String>> t, boolean descending){
		this.this_map = t;
		this.descending = descending;
	}

	@Override
	public int compare(Object i, Object j){
		//rank by frequency, settle ties by using their (presumably) unique ids
		int res;
		if(descending){
			res = computeFreq(this_map.get(j)) - computeFreq(this_map.get(i));
			if(res == 0){
				res = (Integer) j - (Integer) i;
			}
		}
		else{
			res = computeFreq(this_map.get(i)) - computeFreq(this_map.get(j));
			if(res == 0){
				res = (Integer) i - (Integer) j;
			}
		}

		return res;
	}

	private int computeFreq(TreeSet<String> s){
		int res = 0;
		for(String str : s){
			res += Integer.parseInt(str.split("/")[1]);
		}
		return res;
	}

}