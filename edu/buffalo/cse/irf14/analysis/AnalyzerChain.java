package edu.buffalo.cse.irf14.analysis;

import java.util.*;


public class AnalyzerChain implements Analyzer{
	private ArrayList<TokenFilterType> filters;
	private TokenStream currentStream;
	private static int filter_index = 0;
	private TokenFilter currentFilter; 
	TokenFilterFactory currentInstance;

	public AnalyzerChain(TokenStream stream, TokenFilterType... filterTypes){
		currentStream = stream;
		filters = new ArrayList<TokenFilterType>();
		for(TokenFilterType t : filterTypes){
			filters.add(t);
		}
		currentInstance = TokenFilterFactory.getInstance();
	}


	@Override
	public boolean increment(){
		// use filters as steps
		if(filter_index > filters.size()-1){
			//self reset, since the analyzer doesn't provide any reset method
			filter_index = 0;
			return false;
		}
		else{
			currentStream.reset();
			currentFilter = currentInstance.getFilterByType(filters.get(filter_index), currentStream);
			try{
			while(currentFilter.increment()){
				//sing "three blind mice"
			}
			}
			catch(TokenizerException t){
				t.printStackTrace();
			}
			currentStream = currentFilter.getStream();
			filter_index += 1;
			return true;
		}
	}

	@Override
	public TokenStream getStream(){
		return currentStream;
	}

}
