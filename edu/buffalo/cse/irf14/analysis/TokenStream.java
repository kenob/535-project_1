/**
 * 
 */
package edu.buffalo.cse.irf14.analysis;

import java.util.Iterator;
import java.util.ArrayList;

/**
 * @author nikhillo
 * Class that represents a stream of Tokens. All {@link Analyzer} and
 * {@link TokenFilter} instances operate on this to implement their
 * behavior
 */
public class TokenStream implements Iterator<Token>{

	private ArrayList<Token> tokens = new ArrayList<Token>();
	private int current = -1;

	
	/**
	 * Method that checks if there is any Token left in the stream
	 * with regards to the current pointer.
	 * DOES NOT ADVANCE THE POINTER
	 * @return true if at least one Token exists, false otherwise
	 */
	@Override
	public boolean hasNext() {
		// TODO YOU MUST IMPLEMENT THIS
		if(current >= tokens.size() - 1){
			return false;
		}
		else{
			return true;
		}
	}

	/**
	 * Method to return the next Token in the stream. If a previous
	 * hasNext() call returned true, this method must return a non-null
	 * Token.
	 * If for any reason, it is called at the end of the stream, when all
	 * tokens have already been iterated, return null
	 */
	@Override
	public Token next() {
		// TODO YOU MUST IMPLEMENT THIS
		if(hasNext()){
			current += 1;
			Token res = tokens.get(current);
			return res;
		}

		else{
			//questionable, but the tokenstream test won't let this go
			current += 1;
			return null;
		}
	}
	
	/**
	 * Method to remove the current Token from the stream.
	 * Note that "current" token refers to the Token just returned
	 * by the next method. 
	 * Must thus be NO-OP when at the beginning of the stream or at the end
	 */
	@Override
	public void remove() {
		// TODO YOU MUST IMPLEMENT THIS
		if(!(getCurrent()==null)){
			tokens.remove(current);
			reset();
			}		
		
	}
	
	/**
	 * Method to reset the stream to bring the iterator back to the beginning
	 * of the stream. Unless the stream has no tokens, hasNext() after calling
	 * reset() must always return true.
	 */
	public void reset() {
		//TODO : YOU MUST IMPLEMENT THIS
		current = -1;
	}
	
	/**
	 * Method to append the given TokenStream to the end of the current stream
	 * The append must always occur at the end irrespective of where the iterator
	 * currently stands. After appending, the iterator position must be unchanged
	 * Of course this means if the iterator was at the end of the stream and a 
	 * new stream was appended, the iterator hasn't moved but that is no longer
	 * the end of the stream.
	 * @param stream : The stream to be appended
	 */
	public void append(TokenStream stream) {
		//TODO : YOU MUST IMPLEMENT THIS
		if(stream != null){
		stream.reset();
		while(stream.hasNext()){
			tokens.add(stream.next());
	}
	}

	}
	
	/**
	 * Method to get the current Token from the stream without iteration.
	 * The only difference between this method and {@link TokenStream#next()} is that
	 * the latter moves the stream forward, this one does not. 
	 * Calling this method multiple times would not alter the return value of {@link TokenStream#hasNext()}
	 * @return The current {@link Token} if one exists, null if end of stream
	 * has been reached or the current Token was removed
	 */
	public Token getCurrent() {
		//TODO: YOU MUST IMPLEMENT THIS
		if(!hasCurrent()){
			return null;
		}
		else{
			return tokens.get(current);
		}	
	}

	public Boolean hasCurrent(){
		if(current < 0 || current > tokens.size() - 1){
			return false;
		}
		else{
			return true;
		}
	}
	
	public void addToken(Token t){
		tokens.add(t);
	}


	public boolean replaceCurrent(Token t){
		if(hasCurrent()){
		tokens.set(current, t);
		return true;
		}
		else{
			return false;
		}
	}

	//merge the current token with the previous one, used mainly for capitalization filter
	public void mergeCurrent(){
		if(hasCurrent()){
		tokens.get(current - 1).merge(tokens.get(current));
		tokens.remove(current);
		current = current - 1;
		}
		
	}

	public void deletePrevious() {
		// TODO Auto-generated method stub
		if(hasCurrent())
		{
			tokens.remove(current-1);
		}
	}

	public void threeToOne(Token term) {
		if(hasCurrent())
		{
			tokens.set(current - 1, new Token(""));
			tokens.set(current , new Token(""));
			tokens.set(current -2, term);
			tokens.remove(current);
			tokens.remove(current - 1);
			current = current -2;
			
		}
	}
	
	public void twoToOne(Token term) {
		if(hasCurrent())
		{
			tokens.set(current - 1, term);
			tokens.set(current , new Token(""));
			tokens.remove(current);
			current = current -1;
			
		}
	}

	public void splitCurrent(String... s){
		int curr = current;
		remove();
		for(String i:s){
			tokens.add(curr, new Token(i));
			curr++;
		}
	}

	public boolean replacePrevious(Token t) {
		if(hasCurrent()){
		tokens.set(current - 1, t);
		return true;
		}
		else{
			return false;
		}
	}

	public void goPrevious() {
		// TODO Auto-generated method stub
		current = current -1;
	}		
	

}
