package edu.buffalo.cse.irf14.analysis.filters;

import edu.buffalo.cse.irf14.analysis.*;


public class NumbersTokenFilter extends TokenFilter{

	@Override
	public boolean increment() throws TokenizerException{

		if(!currentStream.hasNext()){
			return false;
		}
		else{
			Token curr = currentStream.next();
			String term = curr.toString();
			if(hasDigit(term) && containsSpecialChar(term))
			{
				currentStream.replaceCurrent(new Token(onlySpecialChar(term)));
			}
			else if (isNumber(term) && !term.matches("^\\d{8}$" )){
				currentStream.remove();
			}
			// Here is the point where you implement filtering on the current token, as per the requirements in the documentation
			return true;
		}
	}
	private boolean hasDigit(String token)
	{
		for (char c : token.toCharArray())
		{
			if (Character.isDigit(c))
			{
				return true;
			}
		}
		return false;
	}
	private boolean containsSpecialChar(String term) {
		if(term.contains("%") || term.contains("/"))
			return true;
		return false;
	}
	private String onlySpecialChar(String string) {
		for (int i = 0; i<string.length(); i++)
		{
		if(string.contains("%"))
		{
			return "%";
		}
		else if(string.contains("/"))
		{
			return "/";
		}
		}
		return "";

	}
	private boolean isNumber(String string) {		
		string = removeComma(string);

		if (string.matches("-?\\d+"))
		{
			return true;
		}
		return false;
	}
	private String removeComma(String string) {
		String newString = string;
		for (int i = 0; i<string.length(); i++)
		{
			if(string.charAt(i) == ',' || string.charAt(i) == '.' ){
				newString = string.substring(0, i) + string.substring(i+1);
				break;
			}
				
		}
		return newString;
	}
	//we should move this method to the abstract Tokenfilter class since it's not changing at all ???

	/**
 	* Return the underlying {@link TokenStream} instance
 	* @return The underlying stream
 	*/
 	@Override
	public TokenStream getStream(){
		//return stream; but make sure all operations on it have been completed
		currentStream.reset();
		return currentStream;

	}

	public NumbersTokenFilter(TokenStream stream) {
		super(stream);		
	}
}
