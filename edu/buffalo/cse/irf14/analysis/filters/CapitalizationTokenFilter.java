package edu.buffalo.cse.irf14.analysis.filters;


import edu.buffalo.cse.irf14.analysis.*;

public class CapitalizationTokenFilter extends TokenFilter{

	private Boolean is_cap = false;

	@Override
	public boolean increment() throws TokenizerException{

		if(!currentStream.hasNext()){
			return false;
		}
		else{

			Token curr = currentStream.next();

			//takes care of sentence beginnings
			if(curr.getSOSIndicator()){
			curr = new Token(curr.toString().toLowerCase(), curr.getSOSIndicator());
			currentStream.replaceCurrent(curr);
			}

			String current_string = curr.toString();


			// if both this token and the last begin with caps, merge them
			if(is_cap && Character.isUpperCase(current_string.codePointAt(0))){
				currentStream.mergeCurrent();
			}

			// if this token begins in caps, turn on the switch for above method to see
			if(Character.isUpperCase(current_string.codePointAt(0))){
				is_cap = true;
			}

			// if not, turn it off
			else{
				is_cap = false;
			}

			return true;
		}
	}

	/**
 	* Return the underlying {@link TokenStream} instance
 	* @return The underlying stream
 	*/
 	@Override
	public TokenStream getStream(){
		//return stream; but make sure all operations on it have been completed
		//currentStream.reset();
		return currentStream;

	}

	public CapitalizationTokenFilter(TokenStream stream) {
		super(stream);	
	}
}
