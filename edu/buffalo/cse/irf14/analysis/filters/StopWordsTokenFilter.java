package edu.buffalo.cse.irf14.analysis.filters;

import edu.buffalo.cse.irf14.analysis.*;
import java.util.*;

public class StopWordsTokenFilter extends TokenFilter{
	HashSet<String> stopWordSet = new HashSet<String>();

	@Override
	public boolean increment() throws TokenizerException{

		if(!currentStream.hasNext()){
			return false;
		}
		else{
			Token curr = currentStream.next();

			if(stopWordSet.contains(curr.toString())){
				currentStream.remove();
			}
			// Here is the point where you implement filtering on the current token, as per the requirements in the documentation
			return true;
		}
	}

	//we should move this method to the abstract Tokenfilter class since it"s not changing at all ???

	/**
 	* Return the underlying {@link TokenStream} instance
 	* @return The underlying stream
 	*/
 	@Override
	public TokenStream getStream(){
		//return stream; but make sure all operations on it have been completed
		currentStream.reset();
		return currentStream;

	}

	public StopWordsTokenFilter(TokenStream stream) {
		super(stream);
		String[] s = {"a","able","about","across","after","all","almost","also",
									"am","among","an","and","any","are","as","at","be","because",
									"been","but","by","can","cannot","could","dear","did","do","does",
									"either","else","ever","every","for","from","get","got","had","has",
									"have","he","her","hers","him","his","how","however","i","if","in","into",
									"is","it","its","just","least","let","like","likely","may","me","might","most",
									"must","my","neither","no","nor","not","of","off","often","on","only","or","other",
									"our","own","rather","said","say","says","she","should","since","so","some","than","that",
									"the","their","them","then","there","these","they","this","tis","to","too","twas","us","wants",
									"was","we","were","what","when","where","which","while","who","whom","why","will","with","would","yet",
									"you","your"};
		for(String i : s){
			this.stopWordSet.add(i);
		}
	}
}

