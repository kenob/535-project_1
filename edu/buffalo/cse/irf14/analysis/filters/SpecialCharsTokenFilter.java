package edu.buffalo.cse.irf14.analysis.filters;

import edu.buffalo.cse.irf14.analysis.*;

import java.util.*;

public class SpecialCharsTokenFilter extends TokenFilter{

	//elite group containing chars that have been (or will be) handled by the symbol filter
	static char[] to_exclude = new char[]{'-', '.' ,'\'',' '};
	HashSet<Character> exclude_set = new HashSet<Character>(); 

	@Override
	public boolean increment() throws TokenizerException{

		if(!currentStream.hasNext()){
			return false;
		}
		else{
			// Here is the point where you implement filtering on the current token, as per the requirements in the documentation

			Token curr = currentStream.next();

			String s = curr.toString();

			//email handling
			if(s.contains("@") && s.contains(".")){
				String outputStr = removeChar(s, '@');
				currentStream.replaceCurrent(new Token(outputStr));
			}

			char[] c = s.toCharArray();
			HashSet<Character> exclude_this = this.exclude_set;

			//flag for alphanumeric detection
			boolean b = false;

			//flag for digit detection
			boolean d = false;

			//add '-' to the elite group if there exists a number in the token
			for(char i : c){
					if(Character.isDigit(i)){
						d = true;
					}	
			}
			if(d){
					exclude_this.add('-');
			}
			else{
					exclude_this.remove('-');
			}


			//remove characters that are neither alphabets nor numbers, and do not belong to the elite group
			for(char i : c){
				if(!exclude_this.contains(i)){
					if(!Character.isLetterOrDigit(i)){
					s = s.replace(String.valueOf(i), "");
					}
					else{
						b = true;
					}
				}
			}

			currentStream.replaceCurrent(new Token(s));

			//if it's a token of symbols only, remove it
			if(!b){
				currentStream.remove();
			}
			return true;
		}
	}

	private String removeChar(String string, char ch) {
		String newString = string;
		for (int i = 0; i<string.length(); i++)
		{
			if(string.charAt(i) == ch ){
				newString = string.substring(0, i) + string.substring(i+1);
				break;
			}
				
		}
		return newString;
	}

	//we should move this method to the abstract Tokenfilter class since it's not changing at all ???

	/**
 	* Return the underlying {@link TokenStream} instance
 	* @return The underlying stream
 	*/
 	@Override
	public TokenStream getStream(){
		//return stream; but make sure all operations on it have been completed
		currentStream.reset();
		return currentStream;

	}

	public SpecialCharsTokenFilter(TokenStream stream) {
		super(stream);		
		for(char i:to_exclude){
			exclude_set.add(i);
		}

	}
}