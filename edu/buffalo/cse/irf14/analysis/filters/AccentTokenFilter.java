package edu.buffalo.cse.irf14.analysis.filters;

import java.util.HashMap;
import java.util.Map;

import edu.buffalo.cse.irf14.analysis.*;
	
//implement according to the requirements in the documentation


public class AccentTokenFilter extends TokenFilter{
	Map<String, String> accentMap;



	@Override
	public boolean increment() throws TokenizerException{

		if(!currentStream.hasNext()){
			return false;
		}
		else{
			// Here is the point where you implement filtering on the current token, as per the requirements in the documentation
			Token curr = currentStream.next();
			for (Map.Entry<String, String> entry : accentMap.entrySet()) {
			    String key = entry.getKey();
			    String value = entry.getValue();
				if(curr.toString().contains(key))
				{
					curr = new Token(curr.toString().replace(key, value));
					currentStream.replaceCurrent(curr);
				}
			}
			return true;

		}
	}

	//we should move this method to the abstract Tokenfilter class since it's not changing at all ???

	/**
 	* Return the underlying {@link TokenStream} instance
 	* @return The underlying stream
 	*/
 	@Override
	public TokenStream getStream(){
		//return stream; but make sure all operations on it have been completed
		currentStream.reset();
		return currentStream;

	}

	public AccentTokenFilter(TokenStream stream) {
		super(stream);	
		this.accentMap =  new HashMap<String, String>();
		// À => A
		accentMap.put("\u00C0", "A");
		// Á => A
		accentMap.put("\u00C1", "A");
		// Â => A
		accentMap.put("\u00C2", "A");
		// Ã => A
		accentMap.put("\u00C3", "A");
		// Ä => A
		accentMap.put("\u00C4", "A");
		// Å => A
		accentMap.put("\u00C5", "A");
		// Æ => AE
		accentMap.put("\u00C6", "AE");
		// Ç => C
		accentMap.put("\u00C7", "C");
		// È => E
		accentMap.put("\u00C8", "E");
		// É => E
		accentMap.put("\u00C9", "E");
		// Ê => E
		accentMap.put("\u00CA", "E");
		// Ë => E
		accentMap.put("\u00CB", "E");
		// Ì => I
		accentMap.put("\u00CC", "I");
		// Í => I
		accentMap.put("\u00CD", "I");
		// Î => I
		accentMap.put("\u00CE", "I");
		// Ï => I
		accentMap.put("\u00CF", "I");
		// Ĳ => IJ
		accentMap.put("\u0132", "IJ");
		// Ð => D
		accentMap.put("\u00D0", "D");
		// Ñ => N
		accentMap.put("\u00D1", "N");
		// Ò => O
		accentMap.put("\u00D2", "O");
		// Ó => O
		accentMap.put("\u00D3", "O");
		// Ô => O
		accentMap.put("\u00D4", "O");
		// Õ => O
		accentMap.put("\u00D5", "O");
		// Ö => O
		accentMap.put("\u00D6", "O");
		// Ø => O
		accentMap.put("\u00D8", "O");
		// Œ => OE
		accentMap.put("\u0152", "OE");
		// Þ
		accentMap.put("\u00DE", "TH");
		// Ù => U
		accentMap.put("\u00D9", "U");
		// Ú => U
		accentMap.put("\u00DA", "U");
		// Û => U
		accentMap.put("\u00DB", "U");
		// Ü => U
		accentMap.put("\u00DC", "U");
		// Ý => Y
		accentMap.put("\u00DD", "Y");
		// Ÿ => Y
		accentMap.put("\u0178", "Y");
		// à => a
		accentMap.put("\u00E0", "a");
		// á => a
		accentMap.put("\u00E1", "a");
		// â => a
		accentMap.put("\u00E2", "a");
		// ã => a
		accentMap.put("\u00E3", "a");
		// ä => a
		accentMap.put("\u00E4", "a");
		// å => a
		accentMap.put("\u00E5", "a");
		// æ => ae
		accentMap.put("\u00E6", "ae");
		// ç => c
		accentMap.put("\u00E7", "c");
		// è => e
		accentMap.put("\u00E8", "e");
		// é => e
		accentMap.put("\u00E9", "e");
		// ê => e
		accentMap.put("\u00EA", "e");
		// ë => e
		accentMap.put("\u00EB", "e");
		// ì => i
		accentMap.put("\u00EC", "i");
		// í => i
		accentMap.put("\u00ED", "i");
		// î => i
		accentMap.put("\u00EE", "i");
		// ï => i
		accentMap.put("\u00EF", "i");
		// ĳ => ij
		accentMap.put("\u0133", "ij");
		// ð => d
		accentMap.put("\u00F0", "d");
		// ñ => n
		accentMap.put("\u00F1", "n");
		// ò => o
		accentMap.put("\u00F2", "o");
		// ó => o
		accentMap.put("\u00F3", "o");
		// ô => o
		accentMap.put("\u00F4", "o");
		// õ => o
		accentMap.put("\u00F5", "o");
		// ö => o
		accentMap.put("\u00F6", "o");
		// ø => o
		accentMap.put("\u00F8", "o");
		// œ => oe
		accentMap.put("\u0153", "oe");
		// ß => ss
		accentMap.put("\u00DF", "ss");
		// þ => th
		accentMap.put("\u00FE", "th");
		// ù => u
		accentMap.put("\u00F9", "u");
		// ú => u
		accentMap.put("\u00FA", "u");
		// û => u
		accentMap.put("\u00FB", "u");
		// ü => u
		accentMap.put("\u00FC", "u");
		// ý => y
		accentMap.put("\u00FD", "y");
		// ÿ => y
		accentMap.put("\u00FF", "y");
		// ﬀ => ff
		accentMap.put("\uFB00", "ff");
		// ﬁ => fi
		accentMap.put("\uFB01", "fi");
		// ﬂ => fl
		accentMap.put("\uFB02", "fl");
		// ﬃ => ffi
		accentMap.put("\uFB03", "ffi");
		// ﬄ => ffl
		accentMap.put("\uFB04", "ffl");
		// ﬅ => ft
		accentMap.put("\uFB05", "ft");
		// ﬆ => st
		accentMap.put("\uFB06", "st");
	}
}