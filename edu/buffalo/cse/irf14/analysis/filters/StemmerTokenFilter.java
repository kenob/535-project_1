package edu.buffalo.cse.irf14.analysis.filters;

import edu.buffalo.cse.irf14.analysis.*;
import edu.buffalo.cse.irf14.utilities.PorterStemmer;

public class StemmerTokenFilter extends TokenFilter{

	@Override
	public boolean increment() throws TokenizerException{

		if(!currentStream.hasNext()){
			return false;
		}
		else{
			// Here is the point where you implement filtering on the current token, as per the requirements in the documentation
			Token curr = currentStream.next();
			String string = curr.toString().toLowerCase();

			//Porter Stemmer does not handle cases like gracious and luminous properly, but live and let live :)


			//TO-DO: this should check the whole string
			if(Character.isLetter(string.codePointAt(0))){
			PorterStemmer s = new PorterStemmer();

			//we should create a getter for the termbuffer of the token class to avoid schemes like this
			for(int i = 0; i < string.length(); i++){
				s.add(string.charAt(i));
			}

			s.stem();
			Token replacement = new Token(s.toString());

			currentStream.replaceCurrent(replacement);
		}
		return true;

		}
	}

	//we should move this method to the abstract Tokenfilter class since it's not changing at all ???

	/**
 	* Return the underlying {@link TokenStream} instance
 	* @return The underlying stream
 	*/
 	@Override
	public TokenStream getStream(){
		//return stream; but make sure all operations on it have been completed
		currentStream.reset();
		return currentStream;

	}

	public StemmerTokenFilter(TokenStream stream) {
		super(stream);		
	}
}
