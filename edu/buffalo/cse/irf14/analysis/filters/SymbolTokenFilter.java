package edu.buffalo.cse.irf14.analysis.filters;

import edu.buffalo.cse.irf14.analysis.*;
import java.util.*;

public class SymbolTokenFilter extends TokenFilter{
	static HashMap<String, String> map = new HashMap<String,String>();
	@Override
	public boolean increment() throws TokenizerException{

		if(!currentStream.hasNext()){
			return false;
		}
		else{
			// Here is the point where you implement filtering on the current token, as per the requirements in the documentation

			Token curr = currentStream.next();
			String s = curr.toString();
			Token replacement;
			Boolean is_titlecase = Character.isUpperCase(s.codePointAt(0));

			String r = this.map.get(s.toLowerCase());

			if(r == null){
				r = this.map.get(s);
			}

			if(!(r == null)){
				//revert case folding. probably won't be necessary if queries are tokenized and filtered
				if(is_titlecase){
					StringBuilder st = new StringBuilder(r);
					st.setCharAt(0, Character.toUpperCase(r.charAt(0)));
					r = st.toString();
				}
				replacement = new Token(r);
				currentStream.replaceCurrent(replacement);

			}

			s = currentStream.getCurrent().toString();
			if(s.endsWith("!") || s.endsWith(".") || s.endsWith("?")){
			replacement = new Token(symbolTrim(s, '!','?','.'));
			currentStream.replaceCurrent(replacement);
			}

			s = currentStream.getCurrent().toString();

			replacement = new Token(handle_apostrophes(s));
			currentStream.replaceCurrent(replacement);

			s = currentStream.getCurrent().toString();

			if(s.equals(duplicated_string('-',s))){
				currentStream.remove();
			}

			if(s.contains("-")){
				Boolean digit = false;
				digit = is_there_a_digit(s);
				if(!digit){
					replacement = new Token(s.replace("-", " ").trim());
					currentStream.replaceCurrent(replacement);				}
				}

			return true;
		}

	}

	//we should move this method to the abstract Tokenfilter class since it's not changing at all ???

	/**
 	* Return the underlying {@link TokenStream} instance
 	* @return The underlying stream
 	*/
 	@Override
	public TokenStream getStream(){
		//return stream; but make sure all operations on it have been completed
		currentStream.reset();
		return currentStream;

	}

	public SymbolTokenFilter(TokenStream stream) {
		super(stream);	

		/*TODO: find a way to normalize the slashes. On a second thought, these would all go away with the sea of stopwords
		Possible overlap to take note of building analyzer factories
		*/

		//we should probably move things like this to a seperate file
		this.map.put("'em","them");
		this.map.put("don't","do not");
		this.map.put("hadn't","had not");
		this.map.put("hadn't've","had not have");
		this.map.put("hasn't","has not");
		this.map.put("haven't","have not");
		this.map.put("he'd","he had / he would");
		this.map.put("he'd've","he would have");
		this.map.put("he'll","he shall / he will");
		this.map.put("he's","he has / he is");
		this.map.put("how'd","how did / how would");
		this.map.put("how'll","how will");
		this.map.put("how's","how has / how is / how does");
		this.map.put("I'd","I had / I would");
		this.map.put("I'd've","I would have");
		this.map.put("I'll","I shall / I will");
		this.map.put("I'm","I am");
		this.map.put("I've","I have");
		this.map.put("isn't","is not");
		this.map.put("it'd","it had / it would");
		this.map.put("it'd've","it would have");
		this.map.put("it'll","it shall / it will");
		this.map.put("it's","it has / it is");
		this.map.put("let's","let us");
		this.map.put("ma'am","madam");
		this.map.put("mightn't","might not");
		this.map.put("mightn't've","might not have");
		this.map.put("might've","might have");
		this.map.put("mustn't","must not");
		this.map.put("must've","must have");
		this.map.put("needn't","need not");
		this.map.put("not've","not have");
		this.map.put("o'clock","of the clock");
		this.map.put("shan't","shall not");
		this.map.put("she'd","she had / she would");
		this.map.put("she'd've","she would have");
		this.map.put("She'll","She will");
		this.map.put("she's","she has / she is");
		this.map.put("should've","should have");
		this.map.put("shouldn't","should not");
		this.map.put("shouldn't've","should not have");
		this.map.put("that's","that has / that is");
		this.map.put("there'd","there had / there would");
		this.map.put("there'd've","there would have");
		this.map.put("there're","there are");
		this.map.put("there's","there has / there is");
		this.map.put("they'd","they would");
		this.map.put("they'd've","they would have");
		this.map.put("they'll","they shall / they will");
		this.map.put("they're","they are");
		this.map.put("they've","they have");
		this.map.put("wasn't","was not");
		this.map.put("we'd","we had / we would");
		this.map.put("we'd've","we would have");
		this.map.put("we'll","we will");
		this.map.put("we're","we are");
		this.map.put("we've","we have");
		this.map.put("weren't","were not");
		this.map.put("what'll","what shall / what will");
		this.map.put("what're","what are");
		this.map.put("what's","what has / what is / what does");
		this.map.put("what've","what have");
		this.map.put("when's","when has / when is");
		this.map.put("where'd","where did");
		this.map.put("where's","where has / where is");
		this.map.put("where've","where have");
		this.map.put("who'd","who would / who had");
		this.map.put("who'll","who shall / who will");
		this.map.put("who're","who are");
		this.map.put("who's","who has / who is");
		this.map.put("who've","who have");
		this.map.put("why'll","why will");
		this.map.put("why're","why are");
		this.map.put("why's","why has / why is");
		this.map.put("won't","will not");
		this.map.put("would've","would have");
		this.map.put("wouldn't","would not");
		this.map.put("wouldn't've","would not have");
		this.map.put("y'all","you all");
		this.map.put("y'all'd've","you all should have / you all could have / you all would have");
		this.map.put("you'd","you had / you would");
		this.map.put("you'd've","you would have");
		this.map.put("you'll","you shall / you will");
		this.map.put("you're","you are");
		this.map.put("you've","you have");

	}

	protected boolean is_there_a_digit(String s){
		Boolean digit = false;
		for(int i = 0; i<s.length(); i++){
			if(Character.isDigit(s.charAt(i)))
			digit = true;
		}
		return digit;
	}


	protected String duplicated_string(char c, String s){
		char[] fin = new char[s.length()];
		Arrays.fill(fin, c);
		return new String(fin);
	}


	protected String symbolTrim(String s, char... c){
	HashSet<Character> f = new HashSet<Character>();
	for(char i:c){
		f.add(i);
	}
	while ( f.contains(s.charAt(s.length() - 1))) {
		s = s.substring(0, s.length() - 1);
	}
	return s;

	}


	protected String handle_apostrophes(String s){
	while(s.contains("'")){
		int indexOfSymbol = s.indexOf("'");
		if(s.endsWith("'s")){
			s = s.substring(0,s.length()-2);
		}
		else{
			s = s.replace("'","");
		}
	}
	return s;	
	}


}
