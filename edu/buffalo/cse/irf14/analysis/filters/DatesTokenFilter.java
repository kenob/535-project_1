package edu.buffalo.cse.irf14.analysis.filters;

import java.util.Hashtable;

import edu.buffalo.cse.irf14.analysis.*;

public class DatesTokenFilter extends TokenFilter{
Hashtable<String, String> monthMap;
	@Override
	public boolean increment() throws TokenizerException{

		if(!currentStream.hasNext()){
			return false;
		}
		
		else{
			// Here is the point where you implement filtering on the current token, as per the requirements in the documentation
			Token curr = currentStream.next();
			Token next = null;
			Token time;
			if(curr.toString().contains("-")&& hasDigit(curr.toString()))
			{
				String[] years = fromToYear(curr);
				Token year1 = convertDate(new Token(years[0]));
				Token year2 = convertDate(new Token(years[1]));
				String finalYears = year1.toString() + "-" + year2.toString();
				currentStream.replaceCurrent(new Token(finalYears));
			}
			if(curr.toString().contains(":")&& hasDigit(curr.toString()))
			{
						if(currentStream.hasNext())
						{
							next = currentStream.next();

						
							if(hasAMorPM(next.toString()))
							{
								time = convertTime(curr, next);
								currentStream.twoToOne(time);
							}
							else
							{
								currentStream.goPrevious();
								time = convertTime(curr);
								currentStream.replaceCurrent(time);
							}

						}
						else
						{
							time = convertTime(curr);
							currentStream.replaceCurrent(time);
						}

				return true;
				
			}	
			if ( isMonth(curr.toString()) || hasBCorAD(curr.toString()) || isNumber(curr.toString()))
			{
				if (currentStream.hasNext())
				{
					 next = currentStream.next();

					if ( isMonth(next.toString()) || hasBCorAD(next.toString()) || isNumber(next.toString()))
					{
						if (currentStream.hasNext())
						{
							Token nextNext = currentStream.next();

							if ( isMonth(nextNext.toString()) || hasBCorAD(nextNext.toString()) || isNumber(nextNext.toString()))
							{
								currentStream.threeToOne(convertDate(curr, next, nextNext));
							}
							else
							{
								currentStream.goPrevious();
								currentStream.twoToOne(convertDate(curr, next));
							}
						}
						else
						{
							curr = convertDate(curr, next);
						currentStream.twoToOne(curr);
						}
					}
					else
					{
						curr = convertDate(curr);
						currentStream.replacePrevious(curr);

					}
				}
				else
				{
					curr = convertDate(curr);
					currentStream.replaceCurrent(curr);
				}
				
			}
			
			
			return true;
		}
	}
	
	private String[] fromToYear(Token curr) {
		String term = curr.toString();
		String[] years = term.split("-");
		years[1] = years[0].substring(0,2) + years[1];
		return years;
	}

	private String removePunctuations(String term) {
		String newTerm = term;
		int lastIndex = term.length()-1;
		char[] endPunctuations = {',','!','?','.'};
		for (int i = 0; i<endPunctuations.length; i++){
			if(term.toCharArray()[lastIndex] == endPunctuations[i])
			{
				newTerm = term.substring(0, lastIndex);
				break;
			}
		}
		return  newTerm;
	}

	private Token  convertTime(Token... time) {
		String timeOfDay = "RT";
		String term, tttt = "";
		String endPunctuation = "";
		endPunctuation = findEndPunctuation(time, endPunctuation);

		for (Token tok : time)
		{
			term = tok.toString();
			term = removePunctuations(term);
			if(term.contains("AM") || term.contains("am"))
				{timeOfDay = "AM"; }
			 if(term.contains("PM") || term.contains("pm"))
				{timeOfDay = "PM"; }
			 if(term.contains(":"))
				{tttt = (term);}
		}
		if(hasAMorPM(tttt))
				tttt = tttt.substring(0,tttt.length()-2);
		tttt = toTime(tttt, timeOfDay)+endPunctuation;
		endPunctuation = "";
		return new Token(tttt);
	}

	private String findEndPunctuation(Token [] phrase, String existingPunctuation) {
		String term = "";
		for (Token tok: phrase)
		{
			term = term+tok.toString();
		}
		int lastIndex = term.length()-1;
		String endPunctuation = existingPunctuation;
		char[] endPunctuations = {',','!','?','.'};
		for (int i = 0; i<endPunctuations.length; i++){
			if(term.toCharArray()[lastIndex] == endPunctuations[i])
			{
				endPunctuation = Character.toString(endPunctuations[i]);
				break;
			}
		}
		return  endPunctuation;
	}

	private String toTime(String tttt, String timeOfDay) {
		if (tttt.charAt(1)==':') tttt = "0"+tttt; 
		if(timeOfDay == "RT")
		{	int count = colonCount(tttt);
			if (count==2)
				return tttt;
			else if(count==1)
			{
				tttt = tttt+":00";
			}
		}
		if(timeOfDay == "AM")
		{
			int count = colonCount(tttt);
			if (count==2)
				return tttt;
			else if(count==1)
			{
				tttt = tttt+":00";
			}
			
		}
		if(timeOfDay == "PM")
		{ 
			int count = colonCount(tttt);
			if (count==2)
				return tttt;
			else if(count==1)
			{
				tttt = tttt+":00";
			}
			Integer hour = Integer.parseInt(tttt.substring(0, 2));
			hour = hour+12;
			tttt = hour.toString() + tttt.substring(2);
			
		}
		
			
		
		return tttt;
	}

	private int colonCount(String tttt) {
		int count = 0;
		char[] tChar = tttt.toCharArray();
		for (char ch : tChar)
		{
			if (ch==':')
				count++;
				
		}
		return count;
	}

	private boolean hasAMorPM(String term) {
		if(term.contains("AM") || term.contains("PM") || term.contains("am") || term.contains("pm") )
		{
			return true;
		}
		return false;
	}



	private Token convertDate(Token... cal) {
		String yyyy = "", mm = "", dd = "";
		boolean isBC = false;
		String dateFormat = "";
		String endPunctuation = "";
		endPunctuation = findEndPunctuation(cal, endPunctuation);

		for (Token tok : cal)
		{
			String term = tok.toString();

			term = removePunctuations(term);
			
			 if(term.contains("BC"))
			{
				isBC = true;
				continue;
			}
			if(isYear(term))
			{
				yyyy = toYear(term);
				
				continue;
			}
			else if(isDate(term))
			{
				dd = toDate(term);
				continue;
			}
			else if(isMonth(term));
			{
				mm = toMonth(term);
				continue;
			}
		}
		if(mm == null || mm.equals("")) mm = "01";
		if(dd == null || dd.equals("")) dd = "01";
		if(yyyy == null || yyyy.equals("")) yyyy = "1900";
		
		if(!isBC)  dateFormat = yyyy + mm + dd+endPunctuation;
		else dateFormat = "-"+yyyy+mm+dd+endPunctuation;
		endPunctuation = "";
		return new  Token(dateFormat);
	}

	private String toYear(String term) {
		String result = "";
		if(hasBCorAD(term))
			term = term.substring(0,term.length()-2);
		{
			String format = String.format("%%0%dd", 4);
			result = String.format(format, Integer.parseInt(term));
		}

		return result;
	}

	private String toDate(String term) {
		if (term.matches("^\\d{1}$" ))
		{
			return "0"+term;
		}
		return term;
	}

	private String toMonth(String term) {
		if (monthMap.containsKey(term))
		{
			
			return monthMap.get(term);
		}
		return null;
	}

	private boolean isDate(String term) {
		if ((term.matches("^\\d{2}$") || term.matches("^\\d{1}$")) && Integer.parseInt(term)>=1 && Integer.parseInt(term)<=31 )
		{
			return true;
		}
		return false;
	}


	private boolean isYear(String term ) {
		if ( (((term.matches("^\\d{1}$")) || (term.matches("^\\d{2}$")) || (term.matches("^\\d{3}$")) || (term.matches("^\\d{4}$"))) && (Integer.parseInt(term) > 31)) || term.contains("AD") || term.contains("BC"))
		{
			return true;
		}
		return false;
	}

	private boolean hasBCorAD(String string) {
		if (string.contains("AD") || string.contains("BC"))
		{
			return true;
		}
		return false;
	}

	private boolean isNumber(String string) {
		string = removePunctuations(string);
		if (string.matches("-?\\d+"))
		{
			return true;
		}
		return false;
	}

	private boolean hasDigit(String token)
	{
		for (char c : token.toCharArray())
		{
			if (Character.isDigit(c))
			{
				return true;
			}
		}
		return false;
	}


	private boolean isMonth(String term) {
		if (monthMap.containsKey(term))
		{
			return true;
		}
		else
		return false;
	}

	/**
 	* Return the underlying {@link TokenStream} instance
 	* @return The underlying stream
 	*/
 	@Override
	public TokenStream getStream(){
		//return stream; but make sure all operations on it have been completed
		currentStream.reset();
		return currentStream;

	}

	public DatesTokenFilter(TokenStream stream) {
		super(stream);	
		this.monthMap = new Hashtable<String, String>();
		this.monthMap.put("Jan", "01");
		this.monthMap.put("January", "01");
		this.monthMap.put("Feb", "02");
		this.monthMap.put("February", "02");
		this.monthMap.put("Mar", "03");
		this.monthMap.put("March", "03");
		this.monthMap.put("Apr", "04");
		this.monthMap.put("April", "04");
		this.monthMap.put("May", "05");
		this.monthMap.put("Jun", "06");
		this.monthMap.put("June", "06");
		this.monthMap.put("Jul", "07");
		this.monthMap.put("July", "07");
		this.monthMap.put("Aug", "08");
		this.monthMap.put("August", "08");
		this.monthMap.put("Sep", "09");
		this.monthMap.put("September", "09");
		this.monthMap.put("Oct", "10");
		this.monthMap.put("October", "10");
		this.monthMap.put("Nov", "11");
		this.monthMap.put("November", "11");
		this.monthMap.put("Dec", "12");
		this.monthMap.put("December", "12");
		
		
		
	}
}