/**
 * 
 */
package edu.buffalo.cse.irf14.analysis;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * @author nikhillo
 * Class that converts a given string into a {@link TokenStream} instance
 */
public class Tokenizer {

	private String delimiter;

	/**
	 * Default constructor. Assumes tokens are whitespace delimited
	 */
	public Tokenizer() {
		//TODO : YOU MUST IMPLEMENT THIS METHOD
		delimiter = "\\s+";
	}
	
	/**
	 * Overloaded constructor. Creates the tokenizer with the given delimiter
	 * @param delim : The delimiter to be used
	 */
	public Tokenizer(String delim) {
		//TODO : YOU MUST IMPLEMENT THIS METHOD

		delimiter = delim;
	}
	
	/**
	 * Method to convert the given string into a TokenStream instance.
	 * This must only break it into tokens and initialize the stream.
	 * No other processing must be performed. Also the number of tokens
	 * would be determined by the string and the delimiter.
	 * So if the string were "hello world" with a whitespace delimited
	 * tokenizer, you would get two tokens in the stream. But for the same
	 * text used with lets say "~" as a delimiter would return just one
	 * token in the stream.
	 * @param str : The string to be consumed
	 * @return : The converted TokenStream as defined above
	 * @throws TokenizerException : In case any exception occurs during
	 * tokenization
	 */
	public TokenStream consume(String str) throws TokenizerException {
		//TODO : YOU MUST IMPLEMENT THIS METHOD
		if(str==null || str==""){
			throw new TokenizerException();
		}
		TokenStream tkstream = new TokenStream();
		ArrayList<String> words = new ArrayList<String>(Arrays.asList(str.split(delimiter)));

		Boolean isEnd = true;

		for(int i = 0; i<words.size(); i++){
			//for all words in the list, create a token and append to the TokenStream
			String current = words.get(i);


			Token t = new Token(current,isEnd);
			tkstream.addToken(t);

			isEnd = false;

			//mark this token as the end of a sentence if that is the case
			if(current.endsWith(".") || current.endsWith("!") || current.endsWith("?")){
				isEnd = true;
			}
		}


		return tkstream;
	}
}
