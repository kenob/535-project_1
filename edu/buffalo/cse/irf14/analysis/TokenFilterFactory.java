/**
 * 
 */
package edu.buffalo.cse.irf14.analysis;
import edu.buffalo.cse.irf14.analysis.filters.*;


/**
 * Factory class for instantiating a given TokenFilter
 * @author nikhillo
 *
 */
public class TokenFilterFactory {
	/**
	 * Static method to return an instance of the factory class.
	 * Usually factory classes are defined as singletons, i.e. 
	 * only one instance of the class exists at any instance.
	 * This is usually achieved by defining a private static instance
	 * that is initialized by the "private" constructor.
	 * On the method being called, you return the static instance.
	 * This allows you to reuse expensive objects that you may create
	 * during instantiation
	 * @return An instance of the factory
	 */
	private TokenFilterFactory()
	{

	}
	public static TokenFilterFactory currentInstance;

	public static TokenFilterFactory getInstance() {
		//TODO : YOU MUST IMPLEMENT THIS METHOD
		if(currentInstance==null){
			currentInstance = new TokenFilterFactory();
		}
		return currentInstance;
	}
	
	/**
	 * Returns a fully constructed {@link TokenFilter} instance
	 * for a given {@link TokenFilterType} type
	 * @param type: The {@link TokenFilterType} for which the {@link TokenFilter}
	 * is requested
	 * @param stream: The TokenStream instance to be wrapped
	 * @return The built {@link TokenFilter} instance
	 */
	public TokenFilter getFilterByType(TokenFilterType type, TokenStream stream) {
		//TODO : YOU MUST IMPLEMENT THIS METHOD

		//we could replace this crudeness with some sort of sane mapping mechanism

		if(type==TokenFilterType.CAPITALIZATION){
			return new CapitalizationTokenFilter(stream);
		}

		else if(type==TokenFilterType.DATE){
			return new DatesTokenFilter(stream);
		}
		else if(type==TokenFilterType.NUMERIC){
			return new NumbersTokenFilter(stream);
		}
		else if (type==TokenFilterType.ACCENT){
			return new AccentTokenFilter(stream);
		}
		else if (type==TokenFilterType.STOPWORD){
			return new StopWordsTokenFilter(stream);
		}
		else if (type==TokenFilterType.SYMBOL){
			return new SymbolTokenFilter(stream);
		}
		else if (type==TokenFilterType.STEMMER){
			return new StemmerTokenFilter(stream);
		}

		else if (type==TokenFilterType.SPECIALCHARS){
			return new SpecialCharsTokenFilter(stream);
		}


		// else if statements to cover all other filter implementations
		else{
			return null;
		}
																								

		}
}
